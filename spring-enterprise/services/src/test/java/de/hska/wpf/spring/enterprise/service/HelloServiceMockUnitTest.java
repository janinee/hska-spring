package de.hska.wpf.spring.enterprise.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import de.hska.wpf.spring.enterprise.service.DummyHello;
import de.hska.wpf.spring.enterprise.service.HelloService;

@RunWith(MockitoJUnitRunner.class)
public class HelloServiceMockUnitTest {

	@InjectMocks
	private HelloService helloService;
	
	@Mock
	private DummyHello dummyHello;
	
	@Test
	public void helloWorldWithMock() {
		Mockito.doReturn("Hello").when(dummyHello).getHello();
		Assert.assertEquals("'Hello World must be returned.'", "Hello World", helloService.getHello("World"));
	}
	
	@Test
	public void helloWorldWithMockReturningNull() {
		Assert.assertNull("Null will be returned as the mock does nothing.", dummyHello.getHello());
		Assert.assertEquals("null World", helloService.getHello("World"));
	}
}
