package de.hska.wpf.spring.enterprise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(value = "realHelloService")
public class HelloService implements IHelloService {
	
	@Autowired
	private DummyHello dummyHello;
	
	@Override
	public String getHello(String toGreet) {
		return dummyHello.getHello() + " " + toGreet;
	}

}
