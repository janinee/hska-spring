package de.hska.wpf.spring.enterprise.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@ComponentScan("de.hska.wpf.spring.enterprise")
@PropertySource("classpath:helloworld.properties")
@Configuration
public class AppConfig {
	
}
