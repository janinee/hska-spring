package de.hska.wpf.spring.enterprise.service;

public interface IHelloService {
	
	String getHello(String toGreet);

}
