package de.hska.wpf.spring.enterprise.domain;

import org.junit.Test;

import de.hska.spring.enterprise.domain.Address;

public class AddressTest {

    @Test(expected = IllegalArgumentException.class)
    public void nullValueCheck() {
        @SuppressWarnings("unused")
        final Address address = new Address(null, 0, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyValueCheck() {
        @SuppressWarnings("unused")
        final Address address = new Address("Foostreet", 12, "", "", null);
    }
}