package de.hska.spring.enterprise.domain;

import org.springframework.util.Assert;

public class Address extends AbstractModel {

    private String street, additionalAddressInfo, postalCode, city, country;

    private int number;

    public Address(String street, int number, String postalCode, String city, String country) {
        Assert.hasText(street);
        Assert.hasText(postalCode);
        Assert.hasText(city);
        Assert.hasText(country);

        this.street = street;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.number = number;
    }

    protected Address() {
        // hide default constructor
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAdditionalAddressInfo() {
        return additionalAddressInfo;
    }

    public void setAdditionalAddressInfo(String additionalAddressInfo) {
        this.additionalAddressInfo = additionalAddressInfo;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalAddressInfo == null) ? 0 : additionalAddressInfo
                .hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + number;
        result = prime * result
                + ((postalCode == null) ? 0 : postalCode.hashCode());
        result = prime * result + ((street == null) ? 0 : street.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Address other = (Address) obj;
        if (additionalAddressInfo == null) {
            if (other.additionalAddressInfo != null)
                return false;
        } else if (!additionalAddressInfo.equals(other.additionalAddressInfo))
            return false;
        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (number != other.number)
            return false;
        if (postalCode == null) {
            if (other.postalCode != null)
                return false;
        } else if (!postalCode.equals(other.postalCode))
            return false;
        if (street == null) {
            if (other.street != null)
                return false;
        } else if (!street.equals(other.street))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Adress [street=" + street + ", number=" + number
                + ", additionalAddressInfo=" + additionalAddressInfo
                + ", postalCode=" + postalCode + ", city=" + city
                + ", country=" + country + "]";
    }

}
